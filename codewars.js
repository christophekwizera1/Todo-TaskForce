// Warrios Team
// ------------
// Samuel HATEGEKIMANA
// Christophe K. KWIZERA

// Seconds
function formatDuration(seconds) {
  // Complete this function
  if (!seconds) {
    // if seconds is equal to 0, null or undefined
    return "now";
  }

  const Formater = (value, type) => {
    /**
     * @return {string}
     *
     * which determines if a type
     * gets an (s) or not
     * at the end
     *
     * e.g hours, minutes, etc..
     */
    return `${value} ${value > 1 ? type + "s" : type}`;
  };

  if (seconds < 60) {
    return Formater(seconds, "second");
  } else {
    const minutes = parseInt(seconds / 60);
    let remainderSeconds = seconds % 60;

    if (minutes < 60) {
      // handler minutes
      let result = Formater(minutes, "minute");
      //
      if (remainderSeconds)
        result += ` and ` + Formater(remainderSeconds, "second");
      //
      return result;
    } else {
      //handler Hours
      const hours = parseInt(minutes / 60);
      let remainderMinutes = minutes % 60;
      //

      if (hours < 24) {
        let result = Formater(hours, "hour");
        //
        if (remainderMinutes)
          result += ", " + Formater(remainderMinutes, "minute");
        //
        if (remainderSeconds)
          result += ` and ` + Formater(remainderSeconds, "second");
        //
        return result;
        //
      } else {
        // handler days
        const days = parseInt(hours / 24);
        const remainderHours = hours % 24;

        if (days > 365) {
          // Handler Years
          const years = parseInt(days / 365);
          const remainderDays = days % 365;

          let results = Formater(years, "year");

          if (remainderDays) {
            results += ", " + Formater(remainderDays, "day");
          }

          if (remainderHours) {
            results += ", " + Formater(remainderHours, "hour");
          }

          if (remainderMinutes) {
            let prefix;
            if (!remainderSeconds) prefix = " and ";
            else prefix = ", ";
            results += prefix + Formater(remainderMinutes, "minute");
          }

          if (remainderSeconds) {
            results += " and " + Formater(remainderSeconds, "second");
          }

          return results;
        } else {
          //
          let results = Formater(days, "day");

          if (remainderHours) {
            results += ", " + Formater(remainderHours, "hour");
          }

          if (remainderMinutes) {
            let prefix;
            if (!remainderSeconds) prefix = " and ";
            else prefix = ", ";
            results += prefix + Formater(remainderMinutes, "minute");
          }

          if (remainderSeconds) {
            results += " and " + Formater(remainderSeconds, "second");
          }

          return results;
        }
      }
    }
  }
}

// Walking Distance
function isValidWalk(walk) {
  //insert brilliant code here
  if (walk.length !== 10) return false;

  if (walk[0] !== walk[walk.length - 1]) return false;

  let letters = {};

  for (const one of walk) {
    // check if letters have the same count
    if (!letters[one]) {
      letters[one] = 1;
    } else {
      letters[one] += 1;
    }
  }

  // if found an error - boolean
  let foundAnError = false;

  for (const one of Object.values(letters)) {
    if (one !== 5) {
      foundAnError = true;
    }
  }

  if (!foundAnError) return false;
  //

  return false;
}

// TODO: create a RomanNumerals helper object
class RomanNumeral {
  constructor(){
    this.toRoman = this.toRoman.bind(this)
    this.fromRoman = this.fromRoman.bind(this)
  }

  toRoman(number) {
    // number <-> 1234
    let result = "",
      num = number;
    //
    const variants = [1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1];
    const romans = [
      "M",
      "CM",
      "D",
      "CD",
      "C",
      "XC",
      "L",
      "XL",
      "X",
      "IX",
      "V",
      "IV",
      "I",
    ];
    //
    for (let i = 0; i < variants.length; i++) {
      //
      let value = parseInt(num / variants[i]);

      if (!value) continue;

      for (let j = 0; j < value; j++) {
        result += romans[i];
      }

      num = num % variants[i];
    }
    //
    return result;
  }

  fromRoman(roman) {
    // roman <-> MMCCXLV
    if (!roman) 
      return 0;
    

    const map = new Map([
      ["I", 1],
      ["V", 5],
      ["X", 10],
      ["L", 50],
      ["C", 100],
      ["D", 500],
      ["M", 1000],
    ]);

    let lastIndex = roman.length - 1;
    let result = map.get(roman[ lastIndex ]);

    for(let i = lastIndex ; i > 0 ; i--){
      //
      const curr = map.get(roman[i]);
      const prev = map.get(roman[i - 1]);

      if (prev >= curr) {
        result += prev;
      } else {
        result -= prev;
      }
      // for deep understanding 
      // uncomment the below
      // console.log(i,'++ current vowel ++',roman[i],'== current vowel ==',curr,'++ Previous Vowel++',prev,'== Result  ==',result)
    }

    return result;
  }
}

const { toRoman,fromRoman } = new RomanNumeral();

const RomanNumerals = { toRoman, fromRoman }
//
// console.log(RomanNumerals.toRoman(3245));
// console.log(RomanNumerals.fromRoman("MMMCCXLV"));

function explosiveSum(num){
  let arr = new Array(num).fill(1)
  //
  if(!num)
  return 0;

  let count=1;

  // for(let p=2; p<=num; p++){
    let arrTwo ;
    for(let i=2; i<=num; i++){
      //
      // how many cells make up i
      let cellsCount = 0,CellSum = 0;
      for(let j=0; j < arr.length; j++){
        // if cells are available
        if(cellsCount) break;
        //
        CellSum += arr[j]
        //
        if(CellSum >=i){
          cellsCount = j+1;
        }
      }
      console.log(cellsCount,"********")
      // replacing the cells with i
      arrTwo = [...arr];
      for(let k= arrTwo.length; k>0; k-= cellsCount ){
        if((k - cellsCount) >= 0){
          arrTwo.splice( k - cellsCount , cellsCount )
          arrTwo.push(i);
          console.log('==1==',arrTwo,'====')
          count += 1;
          // replacing the numbers
          // that are no equal to i
          // e.g i = 4, arrTwo = [1,1,1,4] 
          if(i > 2){
            let t=0;
            const arrThree = [...arrTwo];
            // adding one to form two, 
            // e.g i = 4; 
            // arrThree = [1,1,1,4]
            // we get [2,1,4]
            //
            while( t <= arrTwo.length ){
              if(arrThree[t] === arrThree[t+1] && arrThree[t] === 1 && arrThree[t] !== i){
                arrThree.splice(t,1)
                arrThree[t] = 2;
                count += 1;
                console.log('==2==',arrThree,'====')
                //
              }
              
              if(  arrThree[t+1] !== i && arrThree[t+1] === arrThree[t+2] ){
                  const newValue = arrThree[t] + arrThree[t+1];
                  const arrFith = [...arrThree]
                  if(newValue < i){
                    arrFith.splice(t,1)
                    arrFith[t] = newValue;
                    count += 1;
                    console.log('==4==',arrFith,'====')
                  }
                }
              
              t += 1;
            }

            // adding smaller numbers than i, 
            // e.g i = 4; 
            // arrThree = [2,1,4]
            // add 2 & 1 to 3
            //
            let u=0;
            const arrFour = [...arrThree]
            while ( u < arrThree.length ){
              if( arrThree[u+1] !== i ){
                const newValue = arrThree[u] + arrThree[u+1]
                if(newValue < i){
                  arrFour.splice(u,1)
                  arrFour[u] = newValue;
                  count += 1;
                  console.log('==3==',arrFour,'====')
                }
              }
              u += 1;
            }

          }
        }
      }

    }

  return count
}

console.log(explosiveSum(10))
