import { Platform, View } from "react-native"
import { SafeAreaView } from "react-native-safe-area-context"

const ModalHeader:React.FC<any> = (props) => {
    if(Platform.OS === "ios")
      return (
        <View>{props.children}</View>
      )
    else
      return (
        <SafeAreaView>
          {props.children}
        </SafeAreaView>
      )
  }

  export default ModalHeader