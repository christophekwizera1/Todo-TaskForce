import {
  ActivityIndicator,
  Animated,
  Dimensions,
  Easing,
  FlatList,
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View,
} from "react-native";
import React, {
  FunctionComponent,
  useEffect,
  useReducer,
  useRef,
  useState,
} from "react";
import { SafeAreaView } from "react-native-safe-area-context";
import { globalStyles } from "../constants/GlobalStyles";
import { headerIconSize, borderRadius } from "../constants";
import { AntDesign, Ionicons } from "@expo/vector-icons";
import Colors from "../constants/Colors";
import { StatusBar } from "expo-status-bar";
import AddButton from "../components/AddButton";
import { ScreenProps, TaskItem, TaskSchema } from "../types";
import HeaderCard from "../components/HeaderCard";
import Button from "../components/Button";
import TaskComponent from "../components/Task";
import { TaskActions, TaskReducer } from "../redux";
import { initialTasks } from "../constants/tasks";

const { height, width } = Dimensions.get("screen");

const TasksHelpers = [
  {
    label: "Total Tasks",
    count: 0,
  },
]

const HomeScreen: FunctionComponent<ScreenProps> = ({ navigation }) => {
  const [ AllTasks,setAllTasks ] = useState<TaskSchema[]>([]);
  const [ Tasks,setTasks ] = useState<TaskSchema[]>([]);
  const [ loading,_ ] = useState<boolean>(false);
  const [ showFilter,setFilter ] = useState<boolean>(false);
  const [ filterType,setFilterType ] = useState<string>('');
  const [ taskStatus,setStatus ] = useState(TasksHelpers);
  const [ { globalTasks }, dispatch ] = useReducer(TaskReducer, { globalTasks:initialTasks });
  
  const opacify = useRef(new Animated.Value(0)).current;

  useEffect(() => {
    if (globalTasks) {
      setAllTasks(globalTasks)
    } else {
      // dispatch({ type:GETALL })
    }
  }, [globalTasks]);

  useEffect(() => {
    if(!Tasks[0])
      setTasks(AllTasks)
      /**
       * First we set our task header helper
       */
       setStatus((prev:any) => {
        /**
         * @return {array} with respecctive
         * tasks counts in place
         *
         * 1. Total Task
         * 2. Active AllTasks
         * 3. Completed AllTasks
         * 2. High Priority AllTasks
         */
        const totalTask = Tasks?.length;
        // return
        const doneTasks = Tasks?.filter((one) => one.isCompleted);
        const activeTasks = Tasks?.filter((one) => !one.isCompleted);
        const highPriority = Tasks?.filter((one) => one.priority === "High");

        return [
          { ...prev[0], count: totalTask },
          { ...prev[1], count: activeTasks?.length },
          { ...prev[2], count: doneTasks?.length },
          { ...prev[3], count: highPriority?.length },
        ];
      });
  },[AllTasks,Tasks]);

  useEffect(()=>{
    setTasks(AllTasks.filter(one => one.priority?.includes(filterType)))
  },[filterType])


  const fader = (toValue: number) => {
    const options = {
      toValue,
      duration: 500,
      easing: Easing.ease,
      useNativeDriver: true,
    };
    return Animated.timing(opacify, options).start();
  };

  const toggleComplete = (index: number) => {
    /**
     * @value {index} number
     * is used to find a specific task
     * and update it
     */
    setAllTasks((prev) =>
      prev?.map((task, Taskindex) => {
        if (Taskindex === index) {
          return { ...task, isCompleted: !task.isCompleted };
        } else {
          return task;
        }
      })
    );
  };

  const filter = (type:string) => setFilterType(type)

  const Header = (
    <SafeAreaView style={styles.header}>
      <Image
        source={require("../assets/images/IW_logo.png")}
        style={globalStyles.logo}
      />
      <View style={globalStyles.flexer}>
        <AntDesign
          name="search1"
          color={Colors.invertedText}
          size={headerIconSize}
        />
        <View style={globalStyles.spacer} />
        <Ionicons
          name="filter"
          color={Colors.invertedText}
          size={headerIconSize}
          onPress={() => {
            setFilter((prev) => {
              fader(!prev ? 1 : 0);
              return !prev;
            });
          }}
        />
      </View>
      {showFilter && (
        <Animated.View
          style={[
            styles.filter,
            {
              opacity: opacify,
              transform: [
                {
                  translateY: opacify.interpolate({
                    inputRange: [0, 1],
                    outputRange: [-50, 0],
                  }),
                },
              ],
            },
          ]}
        >
          <Text style={styles.filterHeader}>filter by priority</Text>
          <View style={globalStyles.hr} />
          <View style={globalStyles.spacer} />
          <TouchableOpacity onPress={()=>filter('')}>
            <Text style={styles.filterOption}>All</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={()=>filter('Low')}>
            <Text style={styles.filterOption}>Low priority</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={()=>filter('Medium')}>
            <Text style={styles.filterOption}>Medium priority</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={()=>filter('High')}>
            <Text style={styles.filterOption}>High priority</Text>
          </TouchableOpacity>
        </Animated.View>
      )}
    </SafeAreaView>
  );

  const EmptyTasksView = (
    <View style={styles.emptyWrapper}>
      <Text style={styles.emptyHeader}>nothing here</Text>
      <Text style={styles.emptyNote}>Just like your crush's replies</Text>
      <Button
        text="START WITH A NEW TASK"
        styles={{ width: "auto", paddingVertical: 10, paddingHorizontal: 10 }}
        TextStyles={{ fontSize: 13, fontFamily: "Bold" }}
        loading={false}
        onPress={() => {}}
      />
    </View>
  );

  const TaskHelpers = (
    <>
      <View style={globalStyles.flexer}>
        <HeaderCard item={taskStatus[0]} />
        <HeaderCard item={taskStatus[1]} />
      </View>
      <View style={{ flexDirection: "row" }}>
        <HeaderCard item={taskStatus[2]} />
        <HeaderCard item={taskStatus[3]} />
      </View>
    </>
  );

  const Body = (
    <View style={styles.body}>
      <Text style={styles.mainText}>Welcome</Text>
      {TaskHelpers}
      <ScrollView showsVerticalScrollIndicator={false}>
        {!loading ? (
          Tasks[0] ? (
            React.Children.toArray(
              Tasks.map((one, index) => (
                <>
                  <TaskComponent
                    data={one}
                    index={index}
                    toggle={toggleComplete}
                    navigation={navigation}
                  />
                  <View style={globalStyles.hr} />
                </>
              ))
            )
          ) : (
            EmptyTasksView
          )
        ) : (
          <View style={styles.loader}>
            <ActivityIndicator size={30} color={Colors.primary} />
          </View>
        )}
      </ScrollView>
    </View>
  );

  const appendTask = (task:TaskItem) => {
    setAllTasks((prev:any) => [...prev,task])
    setTasks((prev:any) => [...prev,task])
  }

  return (
    <TouchableWithoutFeedback onPress={()=>{ if(showFilter) setFilter(false) }}>
      <View style={styles.screen}>
        {/* The Dark Overaly Block  */}
        <View style={styles.overlay} />
        {/* Sets the Status to White */}
        <StatusBar style="light" />
        {Header}
        {Body}
        <AddButton appendTask={appendTask} navigation={navigation} />
      </View>
    </TouchableWithoutFeedback>
  );
};

export default HomeScreen;

const styles = StyleSheet.create({
  filterOption: {
    fontSize: 15,
    fontFamily: "Regular",
    color: Colors.mainText,
    paddingLeft: "10%",
    paddingBottom: "10%",
  },
  filterHeader: {
    fontFamily: "Bold",
    fontSize: 15,
    width: "100%",
    textAlign: "center",
    textTransform: "uppercase",
    paddingVertical: "10%",
  },
  filter: {
    position: "absolute",
    width: width * 0.45,
    minWidth: width * 0.45,
    backgroundColor: Colors.baseBg,
    elevation: 5,
    top: height * (Platform.OS === "ios" ? 0.1 : 0.13),
    borderRadius,
    right: 0,
    ...globalStyles.shadow,
    // shadowOpacity:1
  },
  emptyNote: {
    fontFamily: "Regular",
    color: Colors.mutedText,
    fontSize: 17,
    marginTop: "2.5%",
    marginBottom: "5%",
  },
  emptyHeader: {
    fontFamily: "Bold",
    color: Colors.mainText,
    textTransform: "uppercase",
    fontSize: 20,
  },
  emptyWrapper: {
    alignItems: "center",
    marginVertical: "30%",
  },
  loader: {
    position: "absolute",
    top: "150%",
    left: "45%",
  },
  mainText: {
    fontFamily: "Bold",
    fontSize: 30,
    marginLeft: "5%",
    marginTop: "5%",
  },
  body: {
    flex: 1,
    backgroundColor: Colors.baseBg,
    borderRadius,
  },
  overlay: {
    position: "absolute",
    width,
    height: "40%",
    left: "-7.5%",
    top: 0,
    backgroundColor: Colors.dark,
  },
  header: {
    marginTop: Platform.OS === "ios" ? "5%" : 0,
    marginBottom: Platform.OS === "ios" ? 0 : "10%",
    ...globalStyles.flexer,
    position: "relative",
    zIndex: 2,
  },
  screen: {
    flex: 1,
    marginHorizontal: "6.5%",
  },
});
