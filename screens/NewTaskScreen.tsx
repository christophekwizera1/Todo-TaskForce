import { Feather } from "@expo/vector-icons";
import {
  Image,
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  TextInput,
  Platform,
  ScrollView,
  Alert
} from "react-native";
import Button from "../components/Button";
import { borderRadius } from "../constants";
import Colors from "../constants/Colors";
import { globalStyles } from "../constants/GlobalStyles";
import { TaskSchema } from "../types";
import RNPickerSelect from "react-native-picker-select";
import React, { useReducer, useState } from "react";
import * as ImagePicker from "expo-image-picker";
import { TaskReducer } from "../redux";
import { initialTasks } from "../constants/tasks";
import { SimpleNotification } from "../components/Alerts";
import Layout from "../constants/Layout";
import ModalHeader from "../components/ModalHeader";

interface Props {
  navigation: any;
  route:any
}

const initialTask: TaskSchema = {
  title: null,
  priority: null,
  createdAt: null,
  updatedAt: null,
  description: null,
  isCompleted: false,
};

const priorities: any = [
  { label: "Low", value: "Low" },
  { label: "Medium", value: "Medium" },
  { label: "High", value: "High" },
];

const ModalScreen: React.FC<Props> = ({ navigation,route }) => {
  const [ image,setImage ] = useState<string>("");
  const [ task, setTask ] = useState<TaskSchema>(initialTask);
  const [ _, dispatch ] = useReducer(TaskReducer, { globalTasks:initialTasks });

  const { appendTask } = route.params
  const { priority, description, title } = task;

  let inputRef: any = {};

  const pickerPlaceholder = {
    label: "Select a priority...",
    value: null,
    color: "#9EA0A4",
  };

  const AddTask = () => {
    if(priority&&title&&description){
      const payload = {
        data:{
          ...task,
          isCompleted:false,
          createdAt: Date.now(),
          updatedAt: Date.now(),
        }
      }
      // dispatch({ type:TaskActions.ADDNEW, payload })
      appendTask(payload.data)
      BackHandler();
    }else{
      Alert.alert("Creation Failure",'Fill in the missing values')
    }
  };

  const handlerChange = (key: string, value: string) => {
    setTask((prev) => ({ ...prev, [key]: value }));
  };

  const BackHandler = () => navigation.goBack();

  const captureImage = async() =>{
    const permimission = await ImagePicker.getCameraPermissionsAsync();

    if(permimission.granted){
      const opt = {
        mediaTypes: ImagePicker.MediaTypeOptions.Images,
        quality: 1,
        allowsEditing: true,
        allowsMultipleSelection: true,
      }
      const image = await ImagePicker.launchCameraAsync(opt)
      //
      if(!image.cancelled)
      setImage(image.uri)
    }else{
      ImagePicker.requestCameraPermissionsAsync().then((res) => {
        if (res.granted) {
          captureImage();
        } else {
          SimpleNotification("Camera Failure", "grant access to capture");
        }
      });
    }
  };

  return (
    <View style={styles.container}>
      <ModalHeader>
        <TouchableOpacity onPress={BackHandler} style={styles.backHandler}>
          <Feather name="chevron-left" color={Colors.mainText} size={25} />
          <Text style={styles.backHandlerText} >
            Back
          </Text>
        </TouchableOpacity>
      </ModalHeader>
      <View style={globalStyles.hr} />
      <View style={globalStyles.spacer} />
      <ScrollView style={styles.form}>
        <Text style={styles.title}>New Task</Text>
        {/*  */}
        <Text style={styles.label}>Profile Image</Text>
        <TouchableOpacity onPress={captureImage} style={styles.imageWrapper}>
          {
            image ?
            <Image source={{ uri:image }} style={styles.image}/>:
            <Image source={require('../assets/images/placeholder.png')} style={[styles.image,{resizeMode:'contain'}]}/>
          }
        </TouchableOpacity>
        {/*  */}
        <Text style={styles.label}>Title</Text>
        <TextInput
          value={title||""}
          multiline={true}
          style={styles.textInput}
          placeholder="Task Title (140 Characters)"
          onChangeText={(val) => handlerChange("title", val)}
        />
        <View style={globalStyles.spacer} />
        {/*  */}
        <Text style={styles.label}>Description</Text>
        <TextInput
          value={description||""}
          multiline={true}
          placeholder="240 Characters"
          style={[styles.textInput, { minHeight: "15%" }]}
          onChangeText={(val) => handlerChange("description", val)}
        />

        <View style={globalStyles.spacer} />
        {/*  */}
        <Text style={styles.label}>Priority</Text>
        <RNPickerSelect
          items={priorities}
          value={priority}
          style={pickerSelectStyles}
          placeholder={pickerPlaceholder}
          onUpArrow={() => inputRef.focus()}
          onDownArrow={() => inputRef.togglePicker()}
          onValueChange={(value) => handlerChange("priority", value)}
          ref={(ref) => (inputRef = ref)}
        />
        <View style={globalStyles.spacer} />
      </ScrollView>
      {/*  */}
      <Button
        loading={false}
        RightIcon={<Feather name="check" size={30} color={'white'} />}
        styles={styles.btn}
        onPress={AddTask}
      />
    </View>
  );
};

export default ModalScreen;

const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 16,
    borderRadius,
    padding: "5%",
    marginVertical: 10,
    color: Colors.mainText,
    marginHorizontal: "10%",
    backgroundColor: Colors.lightBg,
  },
  inputAndroid: {
    fontSize: 16,
    borderRadius,
    marginVertical: 10,
    paddingVertical: 12,
    paddingHorizontal: 10,
    color: Colors.mainText,
    marginHorizontal: "10%",
    backgroundColor: Colors.lightBg, // to ensure the text is never behind the icon
  },
});

const styles = StyleSheet.create({
  form:{
    paddingBottom:'80%'
  },
  image:{ 
    position:'absolute',
    borderRadius,
    width:'100%',
    height:'100%' 
  },
  imageWrapper:{
    width:Layout.width * 0.8,
    height: 150,
    backgroundColor:Colors.lightBg,
    marginHorizontal: "10%",
    position:'relative',
    marginTop: "3%",
    borderRadius,
    overflow:"hidden"
  },
  btn:{ 
    backgroundColor: Colors.dark, 
    position:'absolute',
    right:Layout.width*0.05,
    top: Layout.height*0.8,
    borderRadius:50
  },
  textInput: {
    width: "80%",
    borderRadius,
    minHeight: "8%",
    marginVertical: 10,
    fontFamily: "Regular",
    color: Colors.mainText,
    paddingHorizontal: "5%",
    marginHorizontal: "10%",
    backgroundColor: Colors.lightBg,
  },
  label: {
    marginHorizontal: "10%",
    fontFamily: "Bold",
    marginTop: "5%",
    fontSize: 15,
  },
  dates: {
    fontFamily: "Regular",
    flex: 1,
    fontSize: 15,
  },
  desc: {
    fontFamily: "Regular",
    fontSize: 17,
    color: Colors.mutedText,
    marginHorizontal: "10%",
    marginVertical: "2%",
  },
  title: {
    fontFamily: "Bold",
    fontSize: 30,
    color: Colors.dark,
    marginHorizontal: "10%",
    textTransform: "capitalize",
  },
  backHandlerText:{
    fontFamily: "Bold",
    fontSize: 15,
    opacity:.5,
    color: Colors.mutedText,
  },
  backHandler: {
    flexDirection: "row",
    alignItems: "center",
    paddingBottom: "5%",
    paddingHorizontal: "8%",
    paddingTop: Platform.OS === 'ios' ? '5%' : 0,
  },
  actionBtns: {
    padding: 10,
    backgroundColor: Colors.lightBg,
    borderRadius,
  },
  nav: {
    marginVertical: "5%",
    marginHorizontal: "10%",
  },
  priorityWrapper: {
    backgroundColor: Colors.dark,
    borderRadius: borderRadius * 2,
    paddingVertical: 5,
    paddingHorizontal: 20,
    marginVertical: 10,
    elevation: 3,
    ...globalStyles.shadow,
    shadowOpacity: 0.1,
  },
  priorityText: {
    fontFamily: "Bold",
    color: "white",
    textAlign: "center",
  },
  container: {
    flex: 1,
    backgroundColor: Colors.baseBg,
  },
});
