import { TaskSchema } from "../types";

export const initialTasks: TaskSchema[] | object = [
    {
      title: "washing",
      description:
        "Tempor ut officia pariatur consectetur sit Lorem nulla irure.",
      priority: "Low",
      isCompleted: false,
      createdAt: Date.now(),
      updatedAt: Date.now(),
    },
    {
      title: "Jogging",
      description:
        "Tempor ut officia pariatur consectetur sit Lorem nulla irure.",
      priority: "High",
      isCompleted: true,
      createdAt: Date.now(),
      updatedAt: Date.now(),
    },
    {
      title: "Praying",
      description:
        "Tempor ut officia pariatur consectetur sit Lorem nulla irure.",
      priority: "High",
      isCompleted: false,
      createdAt: Date.now(),
      updatedAt: Date.now(),
    },
    {
      title: "Workout",
      description:
        "Tempor ut officia pariatur consectetur sit Lorem nulla irure.",
      priority: "Medium",
      isCompleted: true,
      createdAt: Date.now(),
      updatedAt: Date.now(),
    },
    {
      title: "PlayStation",
      description:
        "Tempor ut officia pariatur consectetur sit Lorem nulla irure.",
      priority: "Medium",
      isCompleted: false,
      createdAt: Date.now(),
      updatedAt: Date.now(),
    },
  ];
  