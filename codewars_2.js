//
// Christophe Kwizera & Samuel Hategikimana

const makes10=(a,b)=>a==10||b==10||a+b==10
//
const makeBricks=(a,b,n)=>{
    const bs = b*5;
    const sum = bs+a;
    let result = false;
    
    if(sum !== n){
      for(let i=1;i<b;i++){
        if((i*5)+a == 8){
          result = true;
        }
      }
    }else{
      result = true
    }
    
    return result
}
//
const stickOrChopsticks=(things,n)=>things=='stick'?-(n%2)||n/2:n*2
//
const superEven=n=> {
  const superEvens = [];
  // loop to 200
  for(let i=2; i<=200; i+=2 ){
    // check every number ii super even
    const num = String(i)
    let isSuperEven = true;
    for(let j=0; j<num.length; j++){
      if(num[j]%2 != 0){
        isSuperEven = false
      }
    }
    //
    if(isSuperEven)
    superEvens.push(i)
  }
  // display our super evens
  return superEvens[n-1]
  //
}
// const superEven = n => Array(200)[n-1];
//
const countWord=s=>{
    let counter=0, currentLetterIndex=0;
    const template='word';
    const progress = [];
    for(let i=0;i<s.length;i++){
      /**
      * @ returns {index} 
      * @ type {integer}
      * if a number is found in the template
      * else it return -1
      */
      const valid = template.indexOf(s[i].toLowerCase())
      //
      if( valid === -1 )
        continue;
      //
      progress.push(s[i])
      //
      if( progress.length > 1)
        if(progress[0] == 'W' && progress[1] == 'o'){
          progress.length = 0;
          continue;
        }
      //
      if(currentLetterIndex === 3){
        counter++;
        currentLetterIndex = 0;
        progress.length = 0;
      }
      
      if(currentLetterIndex === valid)
        currentLetterIndex ++;
      
    }
    return counter
}
//
const sentenceCompression=s=>[...s].filter(one=>one.match(/[a-z]/i)).join("") 
//
const rectanglePair=p=>[p/4,p/4]
//
const countDown=n=>`${n+2}${n+1}${n}`;
//
const integerAverage=a=>a.reduce((previous,current)=>previous+current,0) % a.length ? false : true
//
function makePizza(pieces) { //Let's make n pieces of Pizza ;-)
    var result = "";
    while ( pieces-- ){
      var needSteps = 6;
      while ( needSteps-- > 1 ) result += make( needSteps );
    }
    return result;
  }
function make(step){
    switch ( step ){
      case 5: return "P";
      case 4: return "i";
      case 3: return "z";
      case 2: return "z";
      case 1: return "a";
    }
}
//
const esperanzasPies=t=>parseInt((t*10)/8)
//
console.log(superEven(5))