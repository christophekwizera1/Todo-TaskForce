/**
 * Learn more about using TypeScript with React Navigation:
 * https://reactnavigation.org/docs/typescript/
 */

import { BottomTabScreenProps } from '@react-navigation/bottom-tabs';
import { CompositeScreenProps, NavigatorScreenParams } from '@react-navigation/native';
import { NativeStackScreenProps } from '@react-navigation/native-stack';

declare global {
  namespace ReactNavigation {
    interface RootParamList extends RootStackParamList {}
  }
}

export type RootStackParamList = {
  Home: undefined;
  TaskScreen: undefined;
  NewTaskScreen: undefined;
  NotFound: undefined;
};

export type RootStackScreenProps<Screen extends keyof RootStackParamList> = NativeStackScreenProps<
  RootStackParamList,
  Screen
>;

export type RootTabParamList = {
  TabOne: undefined;
  TabTwo: undefined;
};

export type RootTabScreenProps<Screen extends keyof RootTabParamList> = CompositeScreenProps<
  BottomTabScreenProps<RootTabParamList, Screen>,
  NativeStackScreenProps<RootStackParamList>
>;

export interface ScreenProps {
  navigation:any 
}

export interface TaskItem {
  label:string,
  count:number
}

export interface TaskCardItem {
  item: TaskItem
}

export interface TaskSchema {
  title:string|null|any,
  description:string|null|any,
  priority:'High'|'Medium'|'Low'|null|any,
  isCompleted:boolean|null|any,
  createdAt:number|null|any,
  updatedAt:number|null|any,
}

export interface TaskAction {
  type: string;
  payload?: { index?:number,data?:object } ;
}
