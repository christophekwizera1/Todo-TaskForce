import { TaskAction, TaskSchema } from "../types";

enum TaskActions {
  ADDNEW = "addnew",
  DELETE = "delete",
  MUTATE = "mutate",
  GETALL = "gettasks"
};

const TaskReducer = (state:any, action: TaskAction) => {
  //
  const { ADDNEW, DELETE, MUTATE, GETALL } = TaskActions;
  const { type, payload } = action;
  //
  
  switch (type) {
    case ADDNEW: {
      /**
       * 
       * @object {payload} {}
       * is appended to other tasks
       * array 
       * 
       * @return {state} array
       */
      return state = {
        ...state,
        globalTasks: [ ...state.globalTasks,action.payload?.data ]
      }
    }
    case DELETE: {
      /**
       * the
      * @params {index} number
      * is used to filter the tasks
      * and drop the match index
      * 
      * @return {state} array
      */
     return state = {
       ...state,
       globalTasks: state.globalTasks.map((_:{},index:number) => index !== payload?.index)
     }
    }
    case MUTATE: {
      /**
       * the payload
       * @params {index} number
       * is used to update the tasks
       * array by mapping
       * 
       * @return {state} array
       */
      return state = {
        ...state,
        globalTasks: state.globalTasks.map((one:{},index:number) => {
          if(index === payload?.index) return one
          else return { ...one,...payload?.data }
        })
      }
    }
    case GETALL: {
        return state.globalTasks
    }
    default: {
        return state
    }
  }
};

export { TaskActions, TaskReducer };
